# coding: utf-8

import numpy as np
import tensorflow as tf
from tqdm import tqdm_notebook
import pandas as pd
from keras.utils import np_utils
import matplotlib.pyplot as plt


# ## Logic Based FizzBuzz Function [Software 1.0]

def fizzbuzz(n):
    # Logic Explanation
    if n % 3 == 0 and n % 5 == 0:
        return 'FizzBuzz'
    elif n % 3 == 0:
        return 'Fizz'
    elif n % 5 == 0:
        return 'Buzz'
    else:
        return 'Other'


# ## Create Training and Testing Datasets in CSV Format
def createInputCSV(start, end, filename):

    # Why list in Python?
    #   1). List is a sequence type in Python. This means that we can run iterations over lists to fetch
    #       the ordered data stored in them.
    #   2). Moreover, a list is mutable. We can append or extend a list easily. Provides for easy manipulation
    #       of stored data.

    inputData = []
    outputData = []

    # Why do we need training Data?
    #   Let us assume our entire neural network model to be a function which has 2 types of inputs:
    #       - The training data
    #       - The weights associated with the input to the hidden layer nodes and the output node.
    #   and which outputs our prediction about a certain problem.
    #   The aim is to start with an arbitrarily chosen set of weights, and over time, tune them
    #   in such a way that the "cost" (The deviation from the correct value) is reduced.
    #   This process of tuning the model over time requires a training data set. A training data set involves a set
    #   of input data with correct output values. At first we find the output of the network to be largely erroneous,
    #   i.e the output received is very different from the correct output in the training data set.
    #   Using methods like gradient descent etc, we slowly change the weights to reduce the cost of this
    #   function for different input values. These different training values constitute the training data set.
    #   This allows us to approach correct predicted values and thus allows us to use this model to predict
    #   the output for a future unknown input.

    for i in range(start, end):
        inputData.append(i)
        outputData.append(fizzbuzz(i))

    # Why Dataframe?
    #   In this step we create a dictionary which stores the training data information. We store the list of numbers
    #   under the key "input" and the list of resultant outputs under the key "label".
    #   The simplest way to represent this data is to store it in a tabular form with two columns "input" and "label".
    #   A DataFrame is a data structure which stores data in a tabular manner with headings for each column
    #   with one or more optional indices.
    #   DataFrame provides methods to convert this data into/from MANY different formats (html, csv, table, string)
    #   Thus we can populate our training data file using DataFrame and then later read from that
    #   file to perform our training.

    dataset = {}
    dataset["input"] = inputData
    dataset["label"] = outputData

    df = pd.DataFrame(dataset)

    # Writing to csv using an inbuilt pandas library method
    df.to_csv(filename)
    print(filename, "Created!")


# ## Processing Input and Label Data

def processData(dataset):
    # Why do we have to process?
    #   The current training data we have is a bunch of natural numbers resulting in a String. A neural network,
    #   which is just a cascade of nodes which symbolically represent biological neurons in our brains,
    #   needs to be able to process this information.
    #   In this specific, our approach to feed in this input of numbers involves converting each natural number
    #   into its binary representation and then feed each node in the input stage a single binary digit from
    #   that converted binary sequence.
    #   At the time, storing the data together in this array also helps in us in using vectorization to process
    #   our data without having to run it iteratively over the individual processed input.
    data = dataset['input'].values
    labels = dataset['label'].values

    processedData = encodeData(data)
    processedLabel = encodeLabel(labels)

    return processedData, processedLabel

def encodeData(data):
    processedData = []


    for dataInstance in data:
        # Why do we have number 10?
        #   As mentioned above in processData, we need to feed the neural network inout layer with the
        #   binary representation of the input number. Since our number range is under 1024,
        #   we can represent it by using 10 binary digits. We shift the number [0..9] times and then taking the
        #   bitwise & with 1 gives us the rightmost digit for each stage in the iteration. We add those digits
        #   to an array and then append that array to the processedData array.

        # digits = []
        # for d in range(0,10,1):
        #     digits.append(dataInstance >> d & 1)

        # processedData.append(digits)
        processedData.append([dataInstance >> d & 1 for d in range(10)])

    return np.array(processedData)


def encodeLabel(labels):
    processedLabel = []

    for labelInstance in labels:
        if (labelInstance == "FizzBuzz"):
            # Fizzbuzz
            processedLabel.append([3])
        elif (labelInstance == "Fizz"):
            # Fizz
            processedLabel.append([1])
        elif (labelInstance == "Buzz"):
            # Buzz
            processedLabel.append([2])
        else:
            # Other
            processedLabel.append([0])

    return np_utils.to_categorical(np.array(processedLabel), 4)


# Create datafiles
createInputCSV(101, 1001, 'training.csv')
createInputCSV(1, 101, 'testing.csv')

# Read Dataset
trainingData = pd.read_csv('training.csv')
testingData = pd.read_csv('testing.csv')

# Process Dataset
processedTrainingData, processedTrainingLabel = processData(trainingData)
processedTestingData, processedTestingLabel = processData(testingData)

# ## Tensorflow Model Definition

# Defining Placeholder
inputTensor = tf.placeholder(tf.float32, [None, 10])
outputTensor = tf.placeholder(tf.float32, [None, 4])

# We choose a two layer, 50 nodes per layer system with a learning rate of 0.09.
NUM_HIDDEN_NEURONS_LAYER_1 = 50
NUM_HIDDEN_NEURONS_LAYER_2 = 50
# Learning rate can be defined as the step with which we scale the gradient while optimising our weights.
LEARNING_RATE = 0.09


# Initializing the weights to Normal Distribution
def init_weights(shape):
    """
    A utility method to populate initial weights.
    :param shape: The shape of the weights tensor.
    :return: The tensor populated by a random distribution.
    """
    return tf.Variable(tf.random_normal(shape, stddev=0.01))


# Initializing the input to hidden layer 1 weights
input_hidden_1_weights = init_weights([10, NUM_HIDDEN_NEURONS_LAYER_1])
# Initializing the hidden layer 1 to hidden layer 2 weights
hidden_1_hidden_2_weights = init_weights([NUM_HIDDEN_NEURONS_LAYER_1, NUM_HIDDEN_NEURONS_LAYER_2])
# Initializing the hidden layer 2 to output layer weights
hidden_2_output_weights = init_weights([NUM_HIDDEN_NEURONS_LAYER_2, 4])

# Computing values at the first hidden layer
# After computing the matrix multiplication
hidden_layer_1 = tf.nn.relu(tf.matmul(inputTensor, input_hidden_1_weights))
# Computing values at the second hidden layer
# After computing the matrix multiplication
hidden_layer_2 = tf.nn.relu(tf.matmul(hidden_layer_1, hidden_1_hidden_2_weights))
# Computing values at the output layer
output_layer = tf.matmul(hidden_layer_2, hidden_2_output_weights)

# Defining Error Function
# Performs the following computations:
# outputTensorSoftmaxed = softmax(outputTensor)  :: Softmax explained in report
# crossEntropy = cross_entropy(outputTensorSoftmaxed, knownOutputTensor)  :: cross entropy explained in report
# error = mean(crossEntropy)
error_function = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=output_layer, labels=outputTensor))

# Defining Learning Algorithm and Training Parameters
# Use the GradientDescentOptimizer to vary the weights in the direction opposite
# to the gradient of the error function with respect to the weight vector in order to minimise the error function.
training = tf.train.AdagradOptimizer(LEARNING_RATE).minimize(error_function)

# Prediction Function
# Get the index with the highest value. That is our prediction classification.
prediction = tf.argmax(output_layer, 1)

# # Training the Model

NUM_OF_EPOCHS = 10000
BATCH_SIZE = 128

training_accuracy = []

with tf.Session() as sess:
    # Set Global Variables?
    #   TensorFlow uses a graph to map the dependencies between various components listed in the model.
    #   We have listed these dependencies above using TensorFlow inbuilt functions. However, these dependencies
    #   need to be initialised to hold the desired computed values. This what global_variables_initializer() does.

    tf.global_variables_initializer().run()

    for epoch in tqdm_notebook(range(NUM_OF_EPOCHS)):

        # Shuffle the Training Dataset at each epoch
        p = np.random.permutation(range(len(processedTrainingData)))
        processedTrainingData = processedTrainingData[p]
        processedTrainingLabel = processedTrainingLabel[p]

        # Start batch training
        # Using a mini batch to train the data and making changes to the weights.
        for start in range(0, len(processedTrainingData), BATCH_SIZE):
            end = start + BATCH_SIZE
            sess.run(training, feed_dict={inputTensor: processedTrainingData[start:end],
                                          outputTensor: processedTrainingLabel[start:end]})
        # Training accuracy for an epoch
        # Keep appending the accuracy in the array. Computed as the mean of the array containing 0s or 1s based on if
        # prediction index is equal to the correct index or not.
        training_accuracy.append(np.mean(np.argmax(processedTrainingLabel, axis=1) ==
                                         sess.run(prediction, feed_dict={inputTensor: processedTrainingData,
                                                                         outputTensor: processedTrainingLabel})))
    # Testing
    # Get the predictedTestLabels by running prediction on the testing data.
    # Gives an array of predicted index values corresponding to the labels.
    predictedTestLabel = sess.run(prediction, feed_dict={inputTensor: processedTestingData})


df = pd.DataFrame()
df['acc'] = training_accuracy
df.plot(subplots=True, grid=True)

def decodeLabel(encodedLabel):
    if encodedLabel == 0:
        return "Other"
    elif encodedLabel == 1:
        return "Fizz"
    elif encodedLabel == 2:
        return "Buzz"
    elif encodedLabel == 3:
        return "FizzBuzz"


# # Testing the Model [Software 2.0]

wrong = 0
right = 0

predictedTestLabelList = []
""
for i, j in zip(processedTestingLabel, predictedTestLabel):
    predictedTestLabelList.append(decodeLabel(j))

    if np.argmax(i) == j:
        right = right + 1
    else:
        wrong = wrong + 1

print("Errors: " + str(wrong), " Correct :" + str(right))

print("Testing Accuracy: " + str(right / (right + wrong) * 100))

plt.show()

# Please input your UBID and personNumber
testDataInput = testingData['input'].tolist()
testDataLabel = testingData['label'].tolist()

testDataInput.insert(0, "UBID")
testDataLabel.insert(0, "anujdutt")

testDataInput.insert(1, "personNumber")
testDataLabel.insert(1, "50292024")

predictedTestLabelList.insert(0, "")
predictedTestLabelList.insert(1, "")

output = {}
output["input"] = testDataInput
output["label"] = testDataLabel

output["predicted_label"] = predictedTestLabelList

opdf = pd.DataFrame(output)
opdf.to_csv('output.csv')

