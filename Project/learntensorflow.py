import tensorflow as tf

x = tf.constant(5, name = "x")

y = tf.Variable(x+5, name="y")

z = tf.constant([[[2, 3, 4], [4, 5, 6]], [[5, 6, 7], [3 ,7, 8]], [[6, 3, 9], [4, 9, 3]], [[6, 3, 9], [4, 9, 3]]], name="z")




print(z)
r = tf.rank(z)


with tf.Session() as session:
    model = tf.global_variables_initializer()
    session.run(model)
    print(session.run(r))
    print(session.run(z))
