# import tensorflow as tf
#
# x = tf.constant(35, name="x")
# y = tf.Variable(x+5, name="y")
#
# model = tf.global_variables_initializer()
#
# with tf.Session() as session:
#     session.run(model)
#     print(session.run(y))
#


import tensorflow as tf

a = [4, 5, 6]

b = []

x = tf.constant([35, 40, 45], name='x')
y = tf.Variable(x + 5, name='y')


model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)
    print(session.run(y))
