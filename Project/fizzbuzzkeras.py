# coding: utf-8

# ## Logic Based FizzBuzz Function [Software 1.0]

# In[25]:


import pandas as pd
import matplotlib.pyplot as plt


def fizzbuzz(n):
    # Logic Explanation
    if n % 3 == 0 and n % 5 == 0:
        return 'FizzBuzz'
    elif n % 3 == 0:
        return 'Fizz'
    elif n % 5 == 0:
        return 'Buzz'
    else:
        return 'Other'


# ## Create Training and Testing Datasets in CSV Format

# In[1]:


def createInputCSV(start, end, filename):
    # Why list in Python?
    inputData = []
    outputData = []

    # Why do we need training Data?
    for i in range(start, end):
        inputData.append(i)
        outputData.append(fizzbuzz(i))

    # Why Dataframe?
    dataset = {}
    dataset["input"] = inputData
    dataset["label"] = outputData

    # Writing to csv
    pd.DataFrame(dataset).to_csv(filename)

    print(filename, "Created!")


# ## Processing Input and Label Data

# In[27]:


def processData(dataset):
    # Why do we have to process?
    data = dataset['input'].values
    labels = dataset['label'].values

    processedData = encodeData(data)
    processedLabel = encodeLabel(labels)

    return processedData, processedLabel


# In[28]:


def encodeData(data):
    processedData = []

    for dataInstance in data:
        # Why do we have number 10?
        processedData.append([dataInstance >> d & 1 for d in range(10)])

    return np.array(processedData)


# In[29]:


from keras.utils import np_utils


def encodeLabel(labels):
    processedLabel = []

    for labelInstance in labels:
        if (labelInstance == "FizzBuzz"):
            # Fizzbuzz
            processedLabel.append([3])
        elif (labelInstance == "Fizz"):
            # Fizz
            processedLabel.append([1])
        elif (labelInstance == "Buzz"):
            # Buzz
            processedLabel.append([2])
        else:
            # Other
            processedLabel.append([0])

    return np_utils.to_categorical(np.array(processedLabel), 4)


# ## Model Definition

# In[30]:


from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import EarlyStopping, TensorBoard

import numpy as np

input_size = 10
drop_out = 0.2
first_dense_layer_nodes = 256
second_dense_layer_nodes = 4


def get_model():
    # Why do we need a model?
    # Why use Dense layer and then activation?
    # Why use sequential model with layers?
    model = Sequential()

    model.add(Dense(first_dense_layer_nodes, input_dim=input_size))
    model.add(Activation('relu'))

    # Why dropout?
    model.add(Dropout(drop_out))

    model.add(Dense(second_dense_layer_nodes))
    model.add(Activation('softmax'))
    # Why Softmax?

    model.summary()

    # Why use categorical_crossentropy?
    model.compile(optimizer='rmsprop',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    return model


# # <font color='blue'>Creating Training and Testing Datafiles</font>

# In[31]:


# Create datafiles
createInputCSV(101, 1001, 'training.csv')
createInputCSV(1, 101, 'testing.csv')

# # <font color='blue'>Creating Model</font>

# In[32]:


model = get_model()

# # <font color = blue>Run Model</font>

# In[33]:


validation_data_split = 0.2
num_epochs = 10000
model_batch_size = 128
tb_batch_size = 32
early_patience = 100

tensorboard_cb = TensorBoard(log_dir='logs', batch_size=tb_batch_size, write_graph=True)
earlystopping_cb = EarlyStopping(monitor='val_loss', verbose=1, patience=early_patience, mode='min')

# Read Dataset
dataset = pd.read_csv('training.csv')

# Process Dataset
processedData, processedLabel = processData(dataset)
history = model.fit(processedData
                    , processedLabel
                    , validation_split=validation_data_split
                    , epochs=num_epochs
                    , batch_size=model_batch_size
                    , callbacks=[tensorboard_cb, earlystopping_cb]
                    )

# # <font color = blue>Training and Validation Graphs</font>

# In[34]:


# get_ipython().run_line_magic('matplotlib', 'inline')
df = pd.DataFrame(history.history)
df.plot(subplots=True, grid=True, figsize=(10, 15))
plt.show()


# # <font color = blue>Testing Accuracy [Software 2.0]</font>

# In[35]:


def decodeLabel(encodedLabel):
    if encodedLabel == 0:
        return "Other"
    elif encodedLabel == 1:
        return "Fizz"
    elif encodedLabel == 2:
        return "Buzz"
    elif encodedLabel == 3:
        return "FizzBuzz"


# In[36]:


wrong = 0
right = 0

testData = pd.read_csv('testing.csv')

processedTestData = encodeData(testData['input'].values)
processedTestLabel = encodeLabel(testData['label'].values)
predictedTestLabel = []

for i, j in zip(processedTestData, processedTestLabel):
    y = model.predict(np.array(i).reshape(-1, 10))
    predictedTestLabel.append(decodeLabel(y.argmax()))

    if j.argmax() == y.argmax():
        right = right + 1
    else:
        wrong = wrong + 1

print("Errors: " + str(wrong), " Correct :" + str(right))

print("Testing Accuracy: " + str(right / (right + wrong) * 100))

# Please input your UBID and personNumber
testDataInput = testData['input'].tolist()
testDataLabel = testData['label'].tolist()

testDataInput.insert(0, "UBID")
testDataLabel.insert(0, "XXXXXXXX")

testDataInput.insert(1, "personNumber")
testDataLabel.insert(1, "XXXXXXXX")

predictedTestLabel.insert(0, "")
predictedTestLabel.insert(1, "")

output = {}
output["input"] = testDataInput
output["label"] = testDataLabel

output["predicted_label"] = predictedTestLabel

opdf = pd.DataFrame(output)
opdf.to_csv('output.csv')

