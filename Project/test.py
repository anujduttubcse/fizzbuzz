import datetime
import pandas as pd
pd.core.common.is_list_like = pd.api.types.is_list_like
import pandas_datareader.data as web
import matplotlib.pyplot as plt
from matplotlib import style

# style.use('fivethirtyeight')
#
# start = datetime.datetime(2010, 1, 1)
# end = datetime.datetime.now()
#
# df = web.DataReader("WIKI/AAPL", "quandl", start, end)
#
# print(df.head())
#
# df['AdjClose'].plot()
#
# plt.show()


web_stats = {'Day':[1,2,3,4,5,6],
             'Visitors':[43,34,65,56,29,76],
             'Bounce Rate':[65,67,78,65,45,52]}

df = pd.DataFrame(web_stats)

print(df.head(2))
print(df.tail(2))

df = df.set_index('Day')

print(df)